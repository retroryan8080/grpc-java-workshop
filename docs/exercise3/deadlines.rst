Deadlines
=========

A frontend often need to make multiple backends calls, or, making backend calls that calls other backends. If you know the performance characteristics of your services, or if you are able to drop requests that takes too long to finish, and return to user something else, then you should explore deadlines.

In gRPC, you can set a deadline to a call, e.g., no more than 1 second in total. When the deadline has exceeded, it'll throw a `StatusRuntimeException` with `Status.DEADLINE_EXCEEDED`. The deadline will propagate across all subsequent nested calls too.  E.g., if you set a deadline to 1 second, and the first call took 0.7s, and if that call makes another nested call, then the nested call would have 0.3s to complete.

To set a deadline is simple, for example, you can decorate a stub with deadline:

.. code-block:: console

  authService.withDeadlineAfter(1, TimeUnit.SECONDS);
    .authenticate(AuthenticationRequest.newBuilder()
    .setUsername(username)
    .setPassword(password)
    .build());

On the server side, if the deadline was exceeded, then the server will also receive a notification. You can listen to call cancellations by attaching a listener to the Context:

.. code-block:: console

  Context.current().addListener(new Context.CancellationListener() {
    @Override
    public void cancelled(Context context) {
      System.out.println("Call was cancelled!");
    }
  }, Executors.newCachedThreadPool());
