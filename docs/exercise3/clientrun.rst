Run the Client
==============

First the top level project needs to be installed locally.  In the root of the project directory run:

.. code-block:: console

  $ mvn install exec:java

Next run the chat client.

The user database is loaded from the file in auth-service/userdatabase.txt.  It defines several users and passwords.

Login as the admin user with a password of qwerty.  You should see the admin has roles `admin` and `user`.

.. code-block:: console

  $ cd chat-cli-client
  $ mvn install exec:java
  ...
  -> /login admin
  Jun 28, 2017 3:28:31 PM com.example.chat.ChatClient readLogin
  INFO: processing login user
  password> qwerty
  Jun 28, 2017 3:28:32 PM com.example.chat.ChatClient authenticate
  INFO: authenticating user: admin
  Jun 28, 2017 3:28:33 PM com.example.chat.ChatClient authenticate
  INFO: user has these roles: [admin, user]
  Jun 28, 2017 3:28:33 PM com.example.chat.ChatClient initChatServices
  INFO: initializing chat services with token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImlzcyI6ImF1dGgtaXNzdWVyIn0.oQebRc48QIig1R5-JajeTCP3TWP1fThBilNpoOtKavA
  [chat message] | /join [room] | /leave [room] | /create [room] | /list | /quit
  -> /quit

Login with an invalid credential, and you should see a `StatusRuntimeException` with `Status.UNAUTHENTICATED` error:

.. code-block:: console

  $ cd chat-cli-client
  $ mvn install exec:java
  ...
  -> /login kevin
  Jun 28, 2017 3:30:46 PM com.example.chat.ChatClient readLogin
  INFO: processing login user
  password> asdf
  Jun 28, 2017 3:30:48 PM com.example.chat.ChatClient authenticate
  INFO: authenticating user: kevin
  Jun 28, 2017 3:30:48 PM com.example.chat.ChatClient authenticate
  SEVERE: user not authenticated: kevin
  io.grpc.StatusRuntimeException: UNAUTHENTICATED
  	at io.grpc.stub.ClientCalls.toStatusRuntimeException(ClientCalls.java:227)
  -> /quit
