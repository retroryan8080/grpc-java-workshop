Setup
=====

This is the Java Version of the workshop.  `A Scala Version of the workshop is here <https://grandcloud.gitlab.io/grpc-scala-workshop/index.html>`_

Pre-Requisites
--------------

* `OpenJDK 8 <http://openjdk.java.net/install/>`_
* `Maven 3+ <https://maven.apache.org/install.html>`_
* A Good Java IDE - IntelliJ or Eclipse
* We’ll be writing Java code!

Getting the Code
----------------

The code is on GitHub, and checkout the initial branch:

.. code-block:: console

  git clone git@github.com:retroryan/grpc-java-chatroom.git
  git checkout initial

Compile
-------

Make sure everything compiles out of the box!

.. code-block:: console

  cd grpc-java-chatroom-workshop
  mvn install


IntelliJ Setup
--------------

You need to add the generted gRPC source files to your project.

1 - After compiling the project verify the protobuf generated sources are under target/generated-sources/protobuf.

2 - Then under the project settings update the auth-service project.

3 - Click on sources and then on the target path navigate down to target/generated-sources/protobuf.

4 - Add both grpc-java and java as source folders.

5 - Do the same thing for chat-service project.
