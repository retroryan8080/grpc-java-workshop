Exercise 5 - Deployment to Kubernetes
=====================================

In this exercise we will deploy the chat service to Kubernetes.

To simplify access to the chat client command line that service is still run locally.




.. toctree::
  :maxdepth: 2

  setup
  deploykubernetes
  usingservices
  deployment
