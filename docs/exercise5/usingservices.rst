Using Kubernetes Services
=========================

In this exercise we are going to modify the server URL's from being hard code to localhost to using ones injected by kubernetes.

This will allow the application to use Kubernetes Service names to discover the other microservices to call.

Setup the Environment File
--------------------------

This code will look for an environment variable for the host name or use a default.

Look at the EnvVars file in  com.example.auth.EnvVars

Kubernetes Deployment Descriptors
---------------------------------

Be sure you have all the kubernetes deployment files in the kubernetes folder.  If they are missing copy them from this folder:

`kubernetes folder <https://github.com/retroryan/grpc-java-chatroom-workshop/tree/solution/kubernetes>`_


Update the Server URLs
----------------------

All of the URLs in the code base need to be updated to use the environment vars.

* Auth Service - Update the port that the auth service starts on from the environment variables:

.. code-block:: console

    Server server = ServerBuilder.forPort(EnvVars.AUTH_SERVICE_PORT)
           .addService(new AuthServiceImpl(repository, "auth-issuer", algorithm))
           .build();


* Chat Service - Update the port that the chat service uses to connect to the auth service to use environment variables:

.. code-block:: console

    final ManagedChannel authChannel = ManagedChannelBuilder.forTarget(EnvVars.AUTH_SERVICE_URL)
         .usePlaintext()
         .build();


* Chat Service - Update the port that the chat service starts on from the environment variables:

.. code-block:: console

    final Server server = ServerBuilder.forPort(EnvVars.CHAT_SERVICE_PORT)
        .addService(chatRoomService)
        .addService(chatStreamService)
        .build();


* Chat Client - Update the port that the chat client uses to connect to the auth service to use environment variables:

.. code-block:: console

  authChannel = ManagedChannelBuilder.forTarget(EnvVars.AUTH_SERVICE_URL)
        .usePlaintext()
        .build();


* Chat Client - Update the port that the chat client uses to connect to the chat service to use environment variables:

.. code-block:: console

  chatChannel = ManagedChannelBuilder.forTarget(EnvVars.CHAT_SERVICE_URL)
      .usePlaintext()
      .build();


Update Database File Location
-----------------------------

Update how the User Repository database file name has been updated in this file to use an environment variable:

In the file and method com.example.auth.repository.UserRepository#getOrCreateFile  change it to be:

.. code-block:: console

    String filePath = EnvVars.DATABASE_PATH + "/" + FILENAME;
    logger.info("loading users database file from path: " + filePath);
    File file = new File(filePath);
