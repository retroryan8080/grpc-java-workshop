Google Cloud Console Setup
==========================

Login to Google Cloud Console with the special workshop credentials:

`https://console.cloud.google.com/home <https://console.cloud.google.com/home>`

Use the provided Google Cloud Lab username and setup the account by:

1 - Dismiss the offer for a free trial and select the project.

2 - Select the only Google Cloud Platform project in the project list. If you don't see a project, let the instructor know!

Google Cloud SDK Install
------------------------

`Setup the Google Cloud SDK <https://cloud.google.com/sdk/>`

Install Cloud SDK
-----------------

.. code-block:: console

  `./google-cloud-sdk/install.sh`

Initialize Cloud SDK
--------------------

.. code-block:: console

  `./google-cloud-sdk/bin/gcloud init`

Login to Cloud
--------------

.. code-block:: console

  `gcloud auth login`

Verify Cloud SDK
----------------

.. code-block:: console

  `gcloud config list`

Install kubectl
---------------

.. code-block:: console

  `gcloud components install kubectl`

If you already have an installation of kubectl in your computer you can skip this step.
