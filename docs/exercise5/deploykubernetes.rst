Deploy a Kubernetes Cluster
===========================

Set Default Region and Zones
----------------------------

.. code-block:: console

  gcloud config set compute/zone us-central1-f
  gcloud config set compute/region us-central1


Configure your Default Project
------------------------------


If you have multiple gcloud accounts then be sure the correct one is specified for the gcloud. You can get your default project id from the command line with:

.. code-block:: console

  gcloud config get-value core/project


If you are in an instructor led class with a provided credential, then you don't need to set another project ID.

If you are not using an instructor provided credentials, then you may need to specify a project ID:

.. code-block:: console

  gcloud config set project your-project-id


Enable Compute Engine and Kubernetes Engine API
-----------------------------------------------

.. code-block:: console

  gcloud services enable \
    compute.googleapis.com \
    container.googleapis.com


Create a Kubernetes Cluster using the Google Kubernetes Engine
--------------------------------------------------------------

Google Kubernetes Engine is Google’s hosted version of Kubernetes.

To create a container cluster execute:

.. code-block:: console

  gcloud container clusters create chatworkshop \
        --cluster-version=1.9.6-gke.1 \
        --num-nodes 3 \
        --machine-type n1-standard-2


Retrieve your credentials for kubectl:

.. code-block:: console

  gcloud container clusters get-credentials chatworkshop


Grant cluster admin permissions to the current user (admin permissions are required to create the necessary RBAC rules for Istio).

.. code-block:: console

  kubectl create clusterrolebinding cluster-admin-binding \
      --clusterrole=cluster-admin \
      --user=$(gcloud config get-value core/account)


Verify kubectl
--------------

.. code-block:: console

  kubectl version

Explanation By Ray Tsang [@saturnism](https://twitter.com/saturnism)
--------------------------------------------------------------------

This will take a few minutes to run. Behind the scenes, it will create Google Compute Engine instances, and configure each instance as a Kubernetes node. These instances don’t include the Kubernetes Master node. In Google Kubernetes Engine, the Kubernetes Master node is managed service so that you don’t have to worry about it!
