Kubernetes Deployment
=====================

These instructions are based on having a running Kubernetes cluster like Google Container Engine.

Optional Build and Upload Docker images
---------------------------------------

These steps are optional if you want to build and upload your own custom docker image.

The docker images for the services have been published on docker hub and can be run from there.  If you want to build the docker images yourself:

1 - Uncommnet the plugin to build the docker images.  In the pom.xml for each sub-project uncomment the following section:

.. code-block:: console

  <plugin>
    <groupId>com.spotify</groupId>
    <artifactId>dockerfile-maven-plugin</artifactId>
  </plugin>

2 - Compile and publish the docker images locally

.. code-block:: console

  mvn install


Verify images were created successfully

.. code-block:: console

  docker images | grep chat-service-java
  REPOSITORY                                                 TAG                                IMAGE ID            CREATED             SIZE
  retroryan/chat-cli-client-java                             1.0                                57a5c8642c56        9 minutes ago       96.9MB
  retroryan/chat-service-java                                1.0                                2347376c323b        9 minutes ago       96.6MB
  retroryan/auth-service-java                                1.0                                555b8e7b5669        10 minutes ago      96.5MB

3 - Tag the docker images

.. code-block:: console

  docker tag retroryan/chat-service-java:1.0 gcr.io/madagascar-204304/chat-service-java:1.0
  docker tag retroryan/auth-service-java:1.0 gcr.io/madagascar-204304/auth-service-java:1.0

4 - Push created tags to <project-id> gcloud container registry

.. code-block:: console

  gcloud docker -- push gcr.io/madagascar-204304/auth-service-java:1.0
  gcloud docker -- push gcr.io/madagascar-204304/chat-service-java:1.0

5 - Verify tags were pushed successfully

.. code-block:: console

  gcloud container images list
  NAME
  gcr.io/madagascar-204304/auth-service-java
  gcr.io/madagascar-204304/chat-service-java
  Only listing images in gcr.io/grpc-workshop. Use --repository to list images in other repositories.


Deploy chatroom services to kubernetes
--------------------------------------

1 - Create the auth service and chat service in kubernetes

.. code-block:: console

  cd kubernetes
  kubectl create -f configmap.yml -f authservice-svc.yml -f authservice-deployment.yml -f chatservice-svc.yml -f chatservice-deployment.yml

2 - Get authservice & chatservice external ips


.. code-block:: console

  kubectl get svc
  NAME           TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)          AGE
  auth-service   LoadBalancer   10.7.243.129   23.251.159.21     9091:30853/TCP   12m
  chat-service   LoadBalancer   10.7.245.69    104.198.242.172   9092:30539/TCP   12m

3 - setup the environment variables to point to the deployed services

NOTE: you must replace authservice & chatservice external ips in the following command
auth-service external ip -> AUTH_SERVICE_HOST
chat-service external ip -> CHAT_SERVICE_HOST

.. code-block:: console

  export AUTH_SERVICE_HOST=146.148.37.164
  export CHAT_SERVICE_HOST=108.59.87.103

Run the client from docker hub
------------------------------

1 - Start two client containers locally in two different terminals

.. code-block:: console

  docker run -it -e DB_PATH=/opt/docker/db \
        -e AUTH_SERVICE_HOST=$AUTH_SERVICE_HOST \
        -e CHAT_SERVICE_HOST=$CHAT_SERVICE_HOST \
        retroryan/chat-cli-client-java:1.0

Optional - Run the client from local docker image
-------------------------------------------------

1 - Start two client containers locally in two different terminals

.. code-block:: console

  docker run -it -e DB_PATH=/opt/docker/db \
          -e AUTH_SERVICE_HOST=$AUTH_SERVICE_HOST \
          -e CHAT_SERVICE_HOST=$CHAT_SERVICE_HOST \
          retroryan/chat-cli-client-java:1.0
