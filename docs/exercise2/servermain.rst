Write the Server main entry
===========================

In `auth-service/src/main/java/AuthServer.java`’s `main()` method:

1. Use `ServerBuilder` to build a new server that listens on port 9091
2. Add an instance of `AuthServiceImpl` as a service: `builder.addService(...)`

.. code-block:: console

  // TODO Use ServerBuilder to create a new Server instance. Start it, and await termination.
  // Algorithm and "auth-issuer" are used for JWT signing and verification.
  Algorithm algorithm = Algorithm.HMAC256("secret");
  Server server = ServerBuilder.forPort(9091)
    .addService(new AuthServiceImpl(repository, "auth-issuer", algorithm))
    .build();

3. Add a shutdown hook to shutdown the server in case the process receives shutdown signal. This will try to shutdown the server gracefully, if shutdown hook is called.

.. code-block:: console

  Runtime.getRuntime().addShutdownHook(new Thread() {
    @Override
    public void run() {
      server.shutdownNow();
    }
  });

4. Start the server:

.. code-block:: console

  server.start();
  logger.info("Server started on port 9091");

5. Server will be running in a background thread. Call server.awaitTermination() to block the main thread

.. code-block:: console

  server.awaitTermination();
