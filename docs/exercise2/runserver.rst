Run the Server
==============

The project has exec-maven-plugin configured to run the main class. To run the server:

.. code-block:: console

  $ cd auth-service
  $ mvn install exec:java
  ...
  INFO: Server started on port 9091

Not bad for the first gRPC service!
