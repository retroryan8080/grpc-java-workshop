Exercise 8 - Deployment of Zipkin to Kubernetes
===============================================

In this exercise we will update the chat service deployment to Kubernetes to use zipkin.

To simplify access to the chat client command line that service is still run locally.




.. toctree::
  :maxdepth: 2

  usingservices
  deployment
