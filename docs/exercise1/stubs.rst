Generate Stubs
==============

Once you have the proto file and the Maven plugin configured, generate the Java class files from Maven

.. code-block:: console

  cd auth-service/
  mvn install

Important: Because the service package is depended by other modules within the project, it’s important to use mvn install to install the artifact so it can be referenced.

All Protobuffer 3 messages gRPC stubs should be generated under target/generated-sources:

.. code-block:: console

  find target/generated-sources
