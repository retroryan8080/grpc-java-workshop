Welcome to gRPC Java Chatroom Workshop!
=======================================

In this workshop, we'll be build a real-time chat client and server w/ gRPC and Java.

`A Scala Version of the workshop is here <https://grandcloud.gitlab.io/grpc-scala-workshop/index.html>`_

.. toctree::
  :maxdepth: 2

  setup/setup
  exercise1/exercise1
  exercise2/exercise2
  exercise3/exercise3
  exercise4/exercise4
  exercise5/exercise5
  exercise6/exercise6
  exercise7/exercise7
  exercise8/exercise8





Authors - Ryan Knight `@knight_cloud <https://twitter.com/knight_cloud>`_ and Ray Tsang `@saturnism <https://twitter.com/saturnism>`_

Based on the original `Workshop of Ray Tsang <http://bit.ly/grpc-java-lab-doc>`_
